package org.gaptap.bamboo.cloudfoundry.client;

import org.junit.Test;
import static org.junit.Assert.assertFalse;

public class ApplicationConfigurationTest {

    @Test
    public void byDefaultNoHostnameIsFalse(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();

        assertFalse(applicationConfiguration.noHostname());
    }

    @Test(expected = IllegalArgumentException.class)
    public void ifNoHostnameIsTrueHostsCannotAlsoBeProvided(){
        ApplicationConfiguration.builder()
                .name("test-app")
                .noHostname(true)
                .host("test-host")
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void ifNoHostnameIsTrueRoutesCannotAlsoBeProvided(){
        ApplicationConfiguration.builder()
                .name("test-app")
                .noHostname(true)
                .route("test-host.cf.com")
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void bothRoutesAndHostsCannotBeProvided(){
        ApplicationConfiguration.builder()
                .name("test-app")
                .route("test-host.cf.com")
                .host("my-host")
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void bothRoutesAndDomainsCannotBeProvided(){
        ApplicationConfiguration.builder()
                .name("test-app")
                .route("test-host.cf.com")
                .domain("my-domain.com")
                .build();
    }
}
